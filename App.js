import React from 'react';
import { StackNavigator, TabNavigator} from 'react-navigation';

import Dashboard from './screens/Dashboard';
import MostActive from './screens/MostActive';
import Portfolio from './screens/Portfolio';
import Block from './screens/Block';
import Test from './screens/Test';

const Tabs = TabNavigator({
  Dashboard: { screen: Dashboard},
  MostActive: {screen: MostActive},
  Portfolio: { screen: Portfolio},
  Test: { screen: Test},

}, {
  tabBarPosition: 'bottom',
  animationEnabled: false,
  tabBarOptions: {
    activeTintColor: '#eeca4a',
    inactiveTintColor: '#eff2ec',
    style: {
      backgroundColor: '#0c1c2c',
      elevation: 1,
    },
    indicatorStyle: {
      backgroundColor: '#ffffff',
      height: 4,
    }
  },
});


const AppStack = StackNavigator({
  Tabs: { screen: Tabs },
  Fetch: { screen: Block},
});


export default AppStack;
