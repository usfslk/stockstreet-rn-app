import React, { Component } from 'react';
  import {
    View,
    Text,
    TouchableHighlight,
    Image,
    StyleSheet,
    FlatList,
  } from 'react-native';

import { RkTheme, RkButton, RkCard, RkText, rkCardContent, rkCardImg, rkCardFooter, RkTextInput} from 'react-native-ui-kitten';
import firebase from 'firebase';
import Icon from 'react-native-fa-icons';

console.ignoredYellowBox = ['Setting a timer'];

class Favorites extends Component {

    constructor(props) {
        super(props)
        this.state = {
            mainData:[],
            keys:[],
        }
      }
     
componentWillMount = () => {
const { currentUser } = firebase.auth();
    firebase.database().ref(`/users/${currentUser.uid}/favorites/`)
    .on('value', snapshot => {
    var obj = snapshot.val()
    var mainData = []
    var keys = []
    for(let a in obj){
        mainData.push(obj[a])
        keys.push(a)
    }
    this.setState({
        userfav : snapshot.val(),
        mainData:mainData,
        keys:keys 
    })
    });
}

saveData = () => {
    this.setState({ loading: true})
    let favdata = this.state.company;
    const { currentUser } = firebase.auth();
    firebase.database().ref(`/users/${currentUser.uid}/favorites/`)
        .push({ favdata })
        .then(() => {
        this.setState({ loading: false})
        alert('Data saved!')
    });
    }
    
    render() {
      return (
        <View style={styles.container}>
        <RkTextInput
        style={styles.textinput}
        placeholder='Search'/>
            {this.state.loading ?
            <FlatList
                data={this.state.mainData}
                keyExtractor={(item, index) => item.favdata}
                renderItem={({ item, index }) => (
                <View style={styles.favcontainer}>
                <Text style={styles.favtext}>{item.favdata}</Text>
                <Icon name='trash' style={{ fontSize: 24, color: '#0c1c2c', paddingRight: 25 }} />
                </View>
                )}
            />
            : null }
        </View>
      );
    }
  }

const styles = StyleSheet.create({
favcontainer: {
    backgroundColor: '#fff',
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 50,
},
favtext: {
    fontSize: 18,
    fontWeight: '400',
    paddingLeft: 25,
    paddingVertical: 9,
},
textinput: {
    backgroundColor: '#fff',
},
});
  
  module.exports = Favorites;
  