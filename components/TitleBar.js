'use strict';
import React, {Component} from 'react';
import ReactNative from 'react-native';
const { StyleSheet, Text, View, Dimensions} = ReactNative;
import Icon from 'react-native-fa-icons';
import { DrawerNavigator, StackNavigator } from 'react-navigation';

var screen = Dimensions.get('window');

export default class TitleBar extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: null, }
}
constructor() {
  super();
  this.state = {
    isOpen: false,
    isDisabled: false,
    swipeToClose: true,
    sliderValue: 0.3
  };
}

  render() {
    return (
        <View style={styles.navbar}>
          <Icon name='bars' style={{ fontSize: 24, color: '#fff', paddingLeft: 25 }} />
           <Text style={styles.navbarTitle}>{this.props.title}</Text>
              <Icon name='info-circle' 
              style={{ fontSize: 24, color: '#fff', paddingRight: 25 }} />

        </View>
    );
  }
}

const styles = StyleSheet.create({
navbar:{
  backgroundColor: '#0c1c2c',
  paddingVertical: 20,
  flexDirection: 'row',
  justifyContent: 'space-between',
},
navbarTitle: {
  textAlign: 'center',
  color: '#fff',
  fontWeight: '600',
  fontSize: 20,
},
});

module.exports = TitleBar;
