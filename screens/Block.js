import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Platform,
  ScrollView,
  View,
  ActivityIndicator,
  Image,
  ListView,
  FlatList,
  Linking,
} from 'react-native';
import { List, ListItem, Button } from "react-native-elements";
import { RkTheme, RkButton, RkCard, RkText, rkCardContent, rkCardImg, rkCardFooter, } from 'react-native-ui-kitten';
import Timestamp from 'react-timestamp';
import Icon from 'react-native-fa-icons';
import firebase from 'firebase';
var Spinner = require('react-native-spinkit');

export default class Block extends Component {

  constructor(props) {

    super(props)
    this.state = {
      error: null,
      loading: true,
      dataloaded: false,
      logoloaded: false,
      peersloaded: false,
      price: '',
      prevclose: '',
      high: '',
      low: '',
      volume: '',
      change: '',
      ceo: '',
      ex: '',
      industry: '',
      marketcap: '',
      week52High: '',
      week52Low: '',
      description: '',
      website: '',
    }
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: `${navigation.state.params.company}`,
      headerStyle: {  height: 60, backgroundColor: "#0c1c2c"},
      headerTitleStyle: { color: '#F6F6F6', textAlign: 'center', fontWeight: '600'},
      headerLeft: <TouchableOpacity
                    style={{paddingLeft: 25}}
                    onPress={ () => { navigation.goBack() }}
                    >
                  <Icon name='angle-left' style={{ fontSize: 24, color: '#fff', paddingRight: 25 }} />
                  </TouchableOpacity>,
    }
  };

saveData = () => {
  this.setState({ loading: true})
  let favdata = this.state.company;
  const { currentUser } = firebase.auth();
  firebase.database().ref(`/users/${currentUser.uid}/favorites/`)
    .push({ favdata })
    .then(() => {
      this.setState({ loading: false})
      alert('Data saved!')
  });
}

componentDidMount() {

  const { state, navigate } = this.props.navigation;

  let userstock = state.params.symbol;
  let path = "https://api.iextrading.com/1.0/stock/";
  let end = "/company";
  let url = path + userstock + end;

  let end2 = "/quote"
  let quoteurl = path + userstock + end2

  let end3 = "/logo"
  let quotelogo = path + userstock + end3

  let end4 = "/news"
  let quotenews = path + userstock + end4

  let end5 = "/company"
  let quoteinfos = path + userstock + end5

  let end6 = "/peers"
  let quotepeers = path + userstock + end6

  fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          symbol: responseJson.symbol,
          company: responseJson.companyName,
          ceo: responseJson.CEO,
          industry: responseJson.industry,
          ex: responseJson.exchange,
        });
      })
      .catch((error) => {
        console.error(error);
      });

      fetch(quoteurl)
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({
              price: responseJson.latestPrice,
              change: responseJson.changePercent,
              prevclose: responseJson.previousClose,
              volume: responseJson.latestVolume,
              high: responseJson.high,
              low: responseJson.low,
              datetime: responseJson.latestUpdatea,
              marketcap: responseJson.marketCap,
              week52High: responseJson.week52High,
              week52Low: responseJson.week52Low,
            });
          })
          .catch((error) => {
            console.error(error);
          });

      fetch(quotelogo)
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({
              logoimg: responseJson.url,
              logoloaded: true,
            });
          })
          .catch((error) => {
            console.error(error);
            this.setState({ logoloaded : false });
          });

          fetch(quoteinfos)
              .then((response) => response.json())
              .then((responseJson) => {
                this.setState({
                  description: responseJson.description,
                  website: responseJson.website,
                });
              })
              .catch((error) => {
                console.error(error);
              });

              fetch(quotenews)
              .then(res => res.json())
              .then(res => {
                this.setState({
                  datanews: res,
                  error: res.error || null,
                  loading: false,
                  dataloaded: true,
                });
              })
              .catch(error => {
                this.setState({ error, loading: false });
              });

              fetch(quotepeers)
              .then(res => res.json())
              .then(res => {
                this.setState({
                  datapeers: res,
                  error: res.error || null,
                  loading: false,
                  peersloaded: true,
                });
              })
              .catch(error => {
                this.setState({ error, loading: false, peersloaded: false });
              });


};

render() {
      RkTheme.setType('RkCard', 'story',  {
        img: {
          height: 75,
          width: 75,
          marginVertical: 25,
          alignSelf: 'center',
        },
        header: {
          alignItems: 'center',
          justifyContent: 'space-between'
        },
      });
      const { state, navigate } = this.props.navigation;
    
    return (

  <ScrollView>

    {this.state.loading ?
      <View style={styles.spinner}>
        <Spinner  isVisible={true} size={90} type={'Bounce'} color={'#0c1c2c'}/>
      </View>
    : null }

    {this.state.dataloaded ? <View>
  
    <RkCard rkType='story' style={styles.card}>
      <View rkCardHeader>
        <RkText rkType='header' style={{fontWeight: 'bold',fontSize: 25,}}>
          ${this.state.price.toString()}
        </RkText>
        <Timestamp time={this.state.datetime} component={Text} />
      </View>
    </RkCard>

    <RkCard rkType='story' style={styles.card}>
    {this.state.logoloaded ?  <View>
    <Image rkCardImg source={{uri: this.state.logoimg}}/>
    </View> : null }
      <View rkCardHeader>
        <RkText rkType='header' style={{fontWeight: 'bold',fontSize: 25,}}><RkText>{this.state.ex.toString()}</RkText></RkText>
        <RkText>{this.state.industry.toString()}</RkText>
      </View>
    </RkCard>

    <RkCard rkType='story' style={styles.card}>
      <View rkCardHeader>
            <Text>
              {this.state.description}
            </Text>
          </View>
    </RkCard>

    <RkCard rkType='story' style={styles.card}>
      <View rkCardHeader>
          <View>
            <Text>
              week52Low: {this.state.week52Low.toString()}
            </Text>
          </View>
          <View>
            <Text>
              week52High: {this.state.week52High.toString()}
            </Text>
          </View>
        </View>
    </RkCard>

    <RkCard style={styles.card}>
      <TouchableOpacity
        onPress={ ()=>{ Linking.openURL(this.state.website)}}>
          <View rkCardHeader>
            <View>
              <Text>Website</Text>
            </View>      
            <View>
              <Icon name='external-link' style={{ fontSize: 24, color: '#0c1c2c'}} />
            </View>   
      </View>
      </TouchableOpacity>
    </RkCard>

  <View style={styles.newspanel}>
    <Text style={styles.newsheader}>Latest News</Text>
      <FlatList
        data={this.state.datanews}
        keyExtractor={(item, index) => item.datetime}
        renderItem={({ item, index }) => (
          <TouchableOpacity
            activeOpacity={0.8}
          onPress={ ()=>{ Linking.openURL(item.url)}}
            >
            <ListItem
              title= {item.headline}
              titleStyle= {{ fontWeight: '600', paddingBottom: 5}}
              subtitle={`${item.source}`}
              subtitleNumberOfLines={0}
              titleNumberOfLines={0}
              containerStyle={{
                  backgroundColor: '#ffffff',
                  borderBottomWidth: 0,
                  }}
            />
          </TouchableOpacity>
        )}
      />
  </View>

    {this.state.peersloaded ? 
    <View style={styles.peerspanel}>
    <FlatList
    data={this.state.datapeers}
    horizontal={true}
    keyExtractor={(item, index) => item}
    renderItem={({ item, index }) => (
      <View style={{marginHorizontal: 5}} >
        <Text style={{ fontWeight: 'bold'}}>{item}</Text>
      </View>
    )}
    />
    </View>
    : null }

  </View> : null}

</ScrollView>
    );
  }
}

const styles = StyleSheet.create({
newspanel : {
  backgroundColor: '#ffffff',
  paddingVertical: 25,
  paddingHorizontal: 25,
  marginVertical: 15,
  marginHorizontal: 25,
  borderRadius: 6,
},
newsheader : {
  fontSize: 20,
  textAlign: 'center',
  marginBottom: 25,
},
card: {
  marginHorizontal: 25, 
  marginTop: 15, 
  paddingHorizontal: 5,
  borderRadius: 6,
},
spinner:{
  alignItems: 'center',
  justifyContent: 'center',
  marginTop: 50,

},
peerspanel: {
  alignItems: 'center',
  backgroundColor: '#ffffff',
  paddingVertical: 25,
  paddingHorizontal: 25,
  marginBottom: 15,
  marginHorizontal: 25,
  borderRadius: 6,
}
});

module.exports = Block
