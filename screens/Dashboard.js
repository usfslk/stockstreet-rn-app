import { DrawerNavigator} from 'react-navigation';
import React, { Component } from "react";
import { View, Text, FlatList, StyleSheet, ScrollView, Dimensions, TouchableOpacity, Linking, StatusBar, Image} from "react-native";
import { List, ListItem, Button } from "react-native-elements";
import { RkTheme, RkButton, RkCard, RkText, rkCardContent, rkCardImg, rkCardFooter, RkTextInput} from 'react-native-ui-kitten';
import TitleBar from "../components/TitleBar";
import {  PublisherBanner } from 'react-native-admob';
import Icon from 'react-native-fa-icons';


var Spinner = require('react-native-spinkit');

class Dashboard extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: null, }
}

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      launchAd: false,
      dataloaded: false,
      data: [],
    };
  }

  makeRemoteRequest = () => {
    const url = `https://newsapi.org/v2/everything?domains=reuters.com&sortBy=Popularity&language=en&apiKey=0670b3b95bf54afdb2ad9d8c7699ee57`;
    this.setState({ loading: true });
    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: res.articles,
          error: res.error || null,
          loading: false,
        });
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  componentDidMount() {
    this.makeRemoteRequest();
    StatusBar.setHidden(true);
  };

  render() {
    const { navigate } = this.props.navigation;
    RkTheme.setType('RkText', 'header',  {
      text: {
        fontWeight: 'bold',
        marginVertical: 10,
      },
    });
    return (
      <View>

      <TitleBar title="Dashboard" />

      <PublisherBanner
      style={{ marginTop: 10, alignSelf: 'center', marginHorizontal: 25}}
      adSize="banner"
      adUnitID="ca-app-pub-8573101599140905/7193651827"
      testDevices={[PublisherBanner.simulatorId]}
      onAppEvent={event => console.log(event.name, event.info)}
      />

      {this.state.loading ?
      <Spinner style={styles.spinner} isVisible={true} size={90} type={'Wave'} color={'#0c1c2c'}/>
      : null }
      

      <View style={styles.container}>

        <FlatList
          data={this.state.data}
          keyExtractor={(item, index) => item.title}
          renderItem={({ item, index }) => (
              
            <RkCard style={styles.card} rkType='story'>
            <Image rkCardImg  source={{uri: item.urlToImage}} />
            <View rkCardHeader>
              <RkText rkType='header'>{item.title}</RkText>
            </View>
            <View rkCardContent>
              <RkText>{item.description}</RkText>
            </View>
            <View rkCardFooter>
              <RkButton rkType='small' style={{backgroundColor: '#0c1c2c'}} >Read</RkButton>
            </View>
          </RkCard>

          )}
        />
      </View>      

      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
  },
  spinner:{
    alignSelf: 'center',
    marginVertical: 25,
  },
  card:{
    marginVertical: 10,
    marginHorizontal: 25,
  }

});


module.exports = Dashboard
