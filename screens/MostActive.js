import { DrawerNavigator, StackNavigator } from 'react-navigation';
import React, { Component } from "react";
import { View, Text, FlatList, StyleSheet, StatusBar, ActivityIndicator, ScrollView, Dimensions, TouchableOpacity} from "react-native";
import { List, ListItem, Button } from "react-native-elements";
import TitleBar from "../components/TitleBar"
var Spinner = require('react-native-spinkit');

class MostActive extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: null, }
}

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      data: [],
      companyList: [],
      error: null,
      refreshing: false,
      dataloaded: false,
    };
  }

  makeRemoteRequest = () => {
    const url = `https://api.iextrading.com/1.0/stock/market/list/mostactive`;
    this.setState({ loading: true });
    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: res,
          companyList: res.symbol,
          error: res.error || null,
          loading: false,
          dataloaded: true,
        });
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

_onPressButton = (symbol) => {
  alert(symbol)
};

  componentDidMount() {
    this.makeRemoteRequest();
    StatusBar.setHidden(true);
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
    <View>

    <TitleBar title="Most Active" />

    {this.state.loading ?
    <Spinner style={styles.spinner} isVisible={true} size={90} type={'Wave'} color={'#0c1c2c'}/>
    : null }

    {this.state.dataloaded ?
    <View style={styles.listcontainer}>
      <FlatList
          data={this.state.data}
          keyExtractor={(item, index) => item.symbol}
          renderItem={({ item, index }) => (
          <TouchableOpacity
           activeOpacity={0.8}
           onPress={() => navigate('Fetch', 
           { symbol: item.symbol, company: item.companyName })}
           >
          <ListItem
            hideChevron
            title= {`${item.companyName}`}
            subtitle= {`[ ${item.symbol} ] ${item.change}% \n${item.primaryExchange}\n${item.sector}`}
            subtitleNumberOfLines={0}
            badge={{ value: '$' + item.latestPrice, textStyle: { color: 'white' }, containerStyle: { backgroundColor: '#000000' } }}
            containerStyle={{backgroundColor: '#ffffff'}}
          />
          </TouchableOpacity>
          )}
        />
    </View>
    : null }

  </View>
    );
  }
}


const styles = StyleSheet.create({
  mainheader: {
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
    marginVertical: 30,
    color: '#FFFFFF'
  },
  listcontainer: {
    backgroundColor: '#F5F5F5',
    marginBottom: 50
  },
  searchicon: {
    marginTop: 20,
    marginRight: 25,
  },
  pagetitle: {
    backgroundColor: '#3498db',
  },
  loading: {
    marginTop: 50,
  },
  spinner:{
    alignSelf: 'center',
    marginVertical: 25,
  },
});


module.exports = MostActive
