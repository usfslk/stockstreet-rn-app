import { DrawerNavigator} from 'react-navigation';
import React, { Component } from "react";
import { StatusBar,
         View,
         Text,
         FlatList,
         StyleSheet,
         ActivityIndicator,
         ScrollView,
         Dimensions,
         TouchableOpacity,
         TextInput,
         Alert,
         AsyncStorage,
         Keyboard,
         ListView,
         } from "react-native";
import { List, ListItem, Button } from "react-native-elements";
import firebase from 'firebase';
import TitleBar from "../components/TitleBar";
import Favorites from "../components/Favorites"
var Spinner = require('react-native-spinkit');
import Icon from 'react-native-fa-icons';
import { RkTextInput} from 'react-native-ui-kitten';
import {  PublisherBanner } from 'react-native-admob';

class Portfolio extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: null, }
}

constructor(props) {
   super(props)
   this.state = {
     currentUser: '',
     error: null,
     UserInput: "",
     loggedIn: null,
     loggedOut: null,
     email: '', password: '',
   }
 }

componentWillMount = () => {
StatusBar.setHidden(true);
this.setState({ loading: true})
firebase.initializeApp({
  apiKey: "AIzaSyAqpqo8aWpC_sJ18CIUvWYUktGMf8w1ylE",
  authDomain: "course-4895c.firebaseapp.com",
  databaseURL: "https://course-4895c.firebaseio.com",
  projectId: "course-4895c",
  storageBucket: "",
  messagingSenderId: "369023837879"
    });
  firebase.auth().onAuthStateChanged((user) => {
    if(user) {
      this.setState({ loggedIn: true, loggedOut: false, loading: false  });
    }
    else {
      this.setState({ loggedIn: false, loggedOut: true, loading: false });
    }
  });
}


onLogIn = () => {
  const { email, password } = this.state;
  this.setState({ error: '', loading: true });
  Keyboard.dismiss()
  firebase.auth().signInWithEmailAndPassword(email, password)
  .then(this.onSuccess.bind(this))
  .catch(() => {
    alert('incorrect username or password')
  })
};

onSignUp = () => {
  const { email, password } = this.state;
  this.setState({ error: '', loading: true });
  Keyboard.dismiss()
  firebase.auth().createUserWithEmailAndPassword(email, password)
  .then(this.onSuccess.bind(this))
  .catch(() => {
    alert('incorrect username or password')
    this.setState({loading: false})
  })
};

onLogOut() {
  firebase.auth().signOut()
};

onSuccess() {
  this.setState({
    email: '',
    password: '',
    loading: false,
    error: ''
  });
};

  render() {
    const { currentUser } = firebase.auth();
    return (
      <View>
        <TitleBar title="Portfolio" />
      <View style={styles.container}>

      {this.state.loggedOut ?
      <Text style={styles.datatext}>Please create an account or sign in to use this feature</Text>
      : null }


      {this.state.loading ?
      <Spinner style={styles.spinner} isVisible={true} size={90} type={'Wave'} color={'#0c1c2c'}/>
      : null }

      {this.state.loggedOut ?
       <View>

        <RkTextInput
        style={styles.textinput}
        placeholder="Email"
        value={this.state.email}
        onChangeText={email => this.setState({ email })}
        />

        <RkTextInput
        style={styles.textinput}
        secureTextEntry={true}
        placeholder="Password"
        value={this.state.password}
        onChangeText={password => this.setState({ password })}
        />

      </View>
       : null }

      {this.state.loggedOut ?
      <View style={styles.authbtn}>
        <TouchableOpacity
          style={styles.button}
          onPress={this.onLogIn}>
          <Text style={styles.textbutton}>Sign in</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.button}
          onPress={this.onSignUp}>
          <Text style={styles.textbutton}>Sign up</Text>
        </TouchableOpacity>

      </View>
      : null }
        
        <TouchableOpacity
      style={styles.button}
      onPress={this.onSignUp}>          
      <View style={styles.rowgoogle}>
      <View><Icon name='google' style={{ fontSize: 24, color: '#fff', paddingRight: 25 }} /></View>  
      <View><Text style={{color: '#fff'}} >Sign in with Google</Text></View>  
      </View>
      </TouchableOpacity>

      <TouchableOpacity
      style={styles.button}
      onPress={this.onSignUp}>          
      <View style={styles.rowtwitter}>
      <View><Icon name='twitter' style={{ fontSize: 24, color: '#fff', paddingRight: 25 }} /></View>  
      <View><Text style={{color: '#fff'}} >Sign in with Twitter</Text></View>  
      </View>
      </TouchableOpacity>


        

      {this.state.loggedIn ?
      <TouchableOpacity
      style={styles.button}
      onPress={this.onLogOut}>
      <Text style={styles.textbutton}>Log Out</Text>
      </TouchableOpacity>
      : null }
      
      {this.state.loggedIn ?
      <Favorites />
      : null }

     </View>

    <PublisherBanner
    style={{ marginTop: 10, alignSelf: 'center', marginHorizontal: 25}}
    adSize="banner"
    adUnitID="ca-app-pub-8573101599140905/7193651827"
    testDevices={[PublisherBanner.simulatorId]}
    onAppEvent={event => console.log(event.name, event.info)}
    />
    
  </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 25,
    justifyContent: 'center',
  },
  textbutton: {
    fontWeight: '400',
    fontSize: 18,
    backgroundColor: '#0c1c2c',
    paddingHorizontal: 60,
    paddingVertical: 15,
    textAlign: 'center',
    color: '#fff',
  },
  datatext: {
    fontSize: 18,
    paddingVertical: 15,
    textAlign: 'center',
    fontWeight: '200',
  },
  loading: {
    margin: 40,
  },
  button:{
    marginBottom: 5,
  },
  authbtn:{
    flexDirection : 'row',
    alignItems: 'stretch',
    justifyContent: 'space-between',
    marginVertical: 7,
  },
  spinner:{
    alignSelf: 'center',
    marginVertical: 25,
  },
  textinput: {
    backgroundColor: '#fff',
},
rowgoogle:{
  flexDirection : 'row',
  backgroundColor: '#34a853',
  paddingHorizontal: 60,
  paddingVertical: 15,
  alignItems: 'center',
  justifyContent: 'center',
  marginVertical: 7,
},
rowtwitter:
{
  backgroundColor: '#1da1f2',
  flexDirection : 'row',
  paddingHorizontal: 60,
  paddingVertical: 15,
  alignItems: 'center',
  justifyContent: 'center',
  marginVertical: 7,
}
});

module.exports = Portfolio
